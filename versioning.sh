#!/bin/bash
function logInfo() {
	START='\033[01;32m'
	END='\033[00;00m'
	MESSAGE=${@:-""}
	echo "${START}${MESSAGE}${END}"
}

function logWarn() {
	START='\033[01;33m'
	END='\033[00;00m'
	MESSAGE=${@:-""}
	echo "${START}${MESSAGE}${END}"
}

function listCsv(){
echo "list buckets on csv"
INPUT=buckets.csv
OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read bucket
do
        echo "bucket : $bucket"
done < $INPUT
IFS=$OLDIFS
}

function enableVersioning(){
echo "enable versioning on buckets"
INPUT=buckets.csv
OLDIFS=$IFS
IFS=','
[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
while read bucket
do      
        echo "bucket : $bucket"
        aws s3api put-bucket-versioning --bucket $bucket --versioning-configuration Status=Enabled

done < $INPUT
IFS=$OLDIFS
}

function listS3(){
echo "list buckets on aws S3 "
aws s3api list-buckets | grep Name
}

logInfo "##########################################";
logInfo "#######   AWS S3 versioning Tool   #######";
logInfo "##########################################";

menu(){
echo "
what you wanna do ?
$(logInfo '1)') List Buckets in csv
$(logInfo '2)') enable versioning on buckets in csv
$(logInfo '3)') List S3 buckets in AWS account 
$(logInfo '0)') Exit
$(logInfo 'Choose an option:') "
        read a
        case $a in
	        1) listCsv ; menu ;;
	        2) enableVersioning ; menu ;;
	        3) listS3 ; menu ;;
		0) exit 0 ;;
		*) echo -e $red"Wrong option."$clear; WrongCommand;;
        esac
}

# Call the menu function
menu

