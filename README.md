# aws-s3-versioning-tool
this is a tool that i created to automate the enable versioning process on S3 buckets using a csv as a source input

## Usage 
check the content of **buckets.csv** file and update it with the buckets name you want to enable version  to

use **aws-saml-auth** or any method of your preference to log into your aws account

to execute the script run 

```
sh versioning.sh
```

![menu](assets/images/menu.png)


select the option you wanna run

